#!/bin/bash

# check if inside SWAN setup
if [[ -z "${SWAN_HOME}" ]] && [[ -z "${USER_ENV_SCRIPT}" ]]; then
    if [ "$#" -ge 1 ];
    then
        EnvironmentName=$1
    else
        EnvironmentName="default"
    fi
    
    SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
      DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
      SOURCE="$(readlink "$SOURCE")"
      [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done

    export DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

else
  EnvironmentName="swan"
  export DIR=$(dirname "$USER_ENV_SCRIPT")
fi

# more stack memory
ulimit -S -s unlimited

if [ "$EnvironmentName" = "default" ]; #Default is python3
then
	setupATLAS
  # lsetup "views LCG_105a x86_64-el9-gcc11-opt"
	lsetup "views LCG_102a x86_64-centos7-gcc11-opt"
	export PATH=${DIR}/bin:${PATH}
	export PYTHONPATH=${DIR}:${PYTHONPATH}	
elif [[ "$EnvironmentName" = "dev" ]];
then
	export PATH=/afs/cern.ch/work/c/chlcheng/local/miniconda/envs/root-latest/bin:$PATH
	export PATH=${DIR}/bin:${PATH}
	export PYTHONPATH=${DIR}:${PYTHONPATH}
elif [[ "$EnvironmentName" = "conda" ]];
then
	echo Entering default conda environment
	export PATH=/afs/cern.ch/work/c/chlcheng/local/miniconda/envs/root-latest/bin:$PATH
elif [[ "$EnvironmentName" = "nightly" ]];
then
setupATLAS
source /cvmfs/sft.cern.ch/lcg/views/dev3/latest/x86_64-centos7-gcc11-opt/setup.sh
	export PATH=${DIR}/bin:${PATH}
	export PYTHONPATH=${DIR}:${PYTHONPATH}	
elif [[ "$EnvironmentName" = "102b" ]];
then
setupATLAS
source /cvmfs/sft.cern.ch/lcg/views/LCG_102b/x86_64-centos7-gcc11-opt/setup.sh
	export PATH=${DIR}/bin:${PATH}
	export PYTHONPATH=${DIR}:${PYTHONPATH}
elif [[ "$EnvironmentName" = "swan" ]];
then
    export PATH=${DIR}/bin:${PATH}
    export PYTHONPATH=${DIR}:${PYTHONPATH}
fi