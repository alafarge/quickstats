import quickstats
quickstats.core.methods._require_module("ROOT", quickstats.core.methods.is_root_installed)

from .basics import *
from .root_object import ROOTObject
from .discrete_nuisance import DiscreteNuisance
from .extended_minimizer import ExtendedMinimizer
from .extended_model import ExtendedModel
from .analysis_object import AnalysisObject
from .asimov_generator import AsimovGenerator
from .asimov_generator import AsimovType
from .asymptotic_cls import AsymptoticCLs
from .likelihood import Likelihood
from .pvalue_toys import PValueToys
from .toy_limit_calculator import ToyLimitCalculator
from .analysis_base import AnalysisBase
from .roo_inspector import RooInspector
from .extended_rfile import ExtendedRFile
from .nuisance_parameter_pull import NuisanceParameterPull
from .nuisance_parameter_ranking import NuisanceParameterRanking
from .nuisance_parameter_harmonizer import NuisanceParameterHarmonizer
from .caching_nll_wrapper import CachingNLLWrapper
from .processors import RooProcessor, RooProcessConfig
#from .signal_modelling import SignalModelling

import ROOT
ROOT.gROOT.SetBatch(True)