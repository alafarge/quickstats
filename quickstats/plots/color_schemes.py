EXTRA_COLORS = {
    "paper": {
        "bg": "#eeeeee",
        "fg": "#444444",
        "bgAlt": "#e4e4e4",
        "red": "#af0000",
        "green": "#008700",
        "blue": "#005f87",
        "yellow": "#afaf00",
        "orange": "#d75f00",
        "pink": "#d70087",
        "purple": "#8700af",
        "lightBlue": "#0087af",
        "olive": "#5f7800"
    },
    "on": {
        "bg": "#1b2b34",
        "fg": "#cdd3de",
        "bgAlt": "#343d46",
        "fgAlt": "#d8dee9",
        "red": "#ec5f67",
        "orange": "#f99157",
        "yellow": "#fac863",
        "green": "#99c794",
        "cyan": "#5fb3b3",
        "blue": "#6699cc",
        "pink": "#c594c5",
        "brown": "#ab7967"
    },
    "series": {
        "cyan": "#54c9d1",
        "orange": "#eca89a",
        "blue": "#95bced",
        "olive": "#ceb776",
        "purple": "#d3a9ea",
        "green": "#9bc57f",
        "pink": "#f0a1ca",
        "turquoise": "#5fcbaa",
        "green": "#00ff26",
        "yellow": "#fbff1f",
        "red": "#a30013"
    },
    "atlas": {
        "onesigma": "#00ff26",
        "twosigma": "#fbff1f"
    },
    "hdbs": {
        "starcommandblue": "#047cbc",
        "spacecadet": "#283044",
        "mintcream": "#ebf5ee",
        "outrageousorange": "#fa7e61",
        "pictorialcarmine": "#ca1551",
        "maroonX11": "#b8336a"
    },
        "hh": {
        "darkpink": "#f2385a",
        "darkblue": "#343844",
        "medturquoise": "#36b1bf",
        "lightturquoise": "#4ad9d9",
        "offwhite": "#e9f1df",
        "darkyellow": "#fdc536",
        "darkgreen": "#125125"
    },
    "traffic_light": {
        "red": "#E9002D",
        "amber": "#FFAA00",
        "green": "#00B000"
    },
    "transparent": "#ffffff00",
    "khaki": "#deba87",
    "celadon": "#85c2a3",
    #"maroon": "#ab0000",
    "tan": "#d2b48c",
    "rust": "#b7410e",
    "navyblue": "#b7410e",
    "mustard": "#ffdb58",
    "charcoal": "#606162",
    "mauve": "#b784a7"
}

QUICKSTATS_PALETTES  = dict(
    darklines=["#000000", "#0019f5", "#377d22", "#ea3423", "#f2a93b", "#7f170e"],
    default=['#000000', '#F2385A', '#4AD9D9', '#FDC536', '#125125', '#E88EED', '#B68D40'],
    hist_series=['#99CCFF', '#EFCF6E', '#64CB34'],
    shading_light=["#FBF0D9", "#C0E7DC", "#E4C0F4", "#B3D4E8"],
    shading_medium=["#E0DCC5", "#C5E00C", "#E0C5D4", "#C5C5E0"],
    shading_dark=["#C1B98A", "#8AC199", "#C18AA9", "#8B8AC1"],
    ibm=["#648fff", "#785ef0", "#dc267f", "#fe6100", "#ffb000"],
    butterflycore=["#f4cccc", "#ffe599", "#b6d7a8", "#9fc5e8", "#9C98DB", "#D6BFAB", "#876659"],
    simple_contrast=["#ED553B", "#20639B", "#3CAEA3", "#F6D55C", "#BF51A0", "#735245"],
    vibrant_contrast=["#dc493a", "#06b8a6", "#4392f0", "#451ea6", "#f57600", "#34a853",
                      "#e6308a", "#686f6c", "#d08b02", "#d1d600"],
    checker=["#068EAC", "#DE5B7D", "#4A9451", "#F97623", "#FAE215", "#BF51A0",
             "#84E4D0", "#B32A32", "#ADE85B", "#FFA41A", "#505BB6", "#596D3D",
             "#2C388E", "#5B3F7B", "#8D89C2", "#6586B3", "#CCA18D", "#735245"],
    atlas_hdbs=["hdbs:spacecadet", "hdbs:starcommandblue", "hdbs:pictorialcarmine",
                "hdbs:outrageousorange", "hdbs:maroonX11", "hdbs:mintcream"],
    atlas_hh=["hh:darkpink", "hh:medturquoise", "hh:darkyellow",
              "hh:darkgreen", "hh:darkblue", "hh:lightturquoise", "hh:offwhite"],
    qualitative=["#FF1F5B", "#00CD6C", "#009ADE", "#AF58BA", "#FF6C1E", "#F28522"]
)